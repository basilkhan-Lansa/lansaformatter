import sublime
import sublime_plugin
import re

class FormatLansaCommand(sublime_plugin.TextCommand):

  indent_commands = '^(begin_com|begin_loop|for|dowhile|if|mthroutine|evtroutine|webroutine|subroutine|case|select)'
  unindent_commands = '^(end_com|end_loop|endfor|endwhile|endif|endroutine|endcase|endselect)'
  neutral_commands = '^(else|when)'

  def format(self, lines):
    formatted_text = ''
    indent_char = '  '
    indent_level = 0

    for line in lines:
      text = self.view.substr(line).strip()
      if re.search(self.unindent_commands, text, re.IGNORECASE):
        indent_level = indent_level - 1
        tmp = (indent_char * indent_level) + text
      elif re.search(self.indent_commands, text, re.IGNORECASE):
        tmp = (indent_char * indent_level) + text
        indent_level = indent_level + 1
      elif re.search(self.neutral_commands, text, re.IGNORECASE):
        tmp = (indent_char * (indent_level - 1)) + text
      else:
        tmp = (indent_char * indent_level) + text

      formatted_text = formatted_text + tmp + '\n'

    return formatted_text

  def run(self, edit):
    file_text = sublime.Region(0, self.view.size())
    lines = self.view.split_by_newlines(file_text)

    self.view.replace(edit, file_text, self.format(lines))